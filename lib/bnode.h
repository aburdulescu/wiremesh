/*
Copyright (C) 2018  Andrei Burdulescu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __WIREMESH_BNODE_H__
#define __WIREMESH_BNODE_H__

#include "common.h"
#include "bdata.h"

enum BType
{
    BTYPE_UNKNOWN,
    BTYPE_INTEGER,
    BTYPE_STRING,
    BTYPE_LIST,
    BTYPE_DICTIONARY
};

struct BNode
{
    struct BNode *child, *sibling;
    enum BType    type;
    struct BData  data;
};

struct BNode* wm_bnode_new();
void          wm_bnode_delete(struct BNode *bn);
void          wm_bnode_print(struct BNode *root);

// Get the bnode of the given key(has to be the child of the given root)
struct BNode* wm_bnode_get_bnode_by_key(const struct BNode *root, const char *key);

#endif

