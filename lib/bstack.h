/*
Copyright (C) 2018  Andrei Burdulescu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __WIREMESH_BSTACK__
#define __WIREMESH_BSTACK__

#include <stdio.h>
 
struct BStack
{
    struct BStack* next;
    void*          data;
};

struct BStack* wm_bstack_new();
void           wm_bstack_delete(struct BStack *st);
void           wm_bstack_push(struct BStack **st, void* data);
void*          wm_bstack_pop(struct BStack **st);
void*          wm_bstack_peek(struct BStack *st);
int            wm_bstack_is_empty(struct BStack *st);

#endif
