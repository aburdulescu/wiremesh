/*
Copyright (C) 2018  Andrei Burdulescu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "bstack.h"

#include <stdlib.h>

static struct BStack* new_node(void *data, struct BStack *next);

struct BStack* wm_bstack_new()
{
    return new_node(0, 0);
}

void wm_bstack_delete(struct BStack *st)
{
    struct BStack *i = st;
    while(i != NULL)
    {
        struct BStack *tmp = i;
        i = i->next;
        free(tmp);
    }
}

void wm_bstack_push(struct BStack **st, void* data)
{
    struct BStack *tmp = new_node(data, *st);
    *st = tmp;
}

void* wm_bstack_pop(struct BStack **st)
{
    if(wm_bstack_is_empty(*st))
        return 0;
    struct BStack *tmp = *st;
    *st = (*st)->next;
    void* popped = tmp->data;
    free(tmp);

    return popped;
}

void* wm_bstack_peek(struct BStack *st)
{
    if(wm_bstack_is_empty(st))
        return 0;
    return st->data;
}

int wm_bstack_is_empty(struct BStack *st)
{
    return !st->next;
}

struct BStack* new_node(void *data, struct BStack *next)
{
    struct BStack *tmp;
    tmp = (struct BStack*)malloc(sizeof(struct BStack));
    if(!tmp)
        exit(0);
    tmp->data = data;
    tmp->next = next;
    return tmp;
}
