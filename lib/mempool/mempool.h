/*
Copyright (C) 2018  Andrei Burdulescu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __WIREMESH_MEMPOOL_H__
#define __WIREMESH_MEMPOOL_H__

struct MempoolBlock
{
    struct MempoolBlock *next;
};

struct Mempool
{
    void                *mem;
    struct MempoolBlock *mem_block;
};

struct Mempool* wm_mempool_create(int num_blocks, int block_size);
void            wm_mempool_destroy(struct Mempool *mp);
void*           wm_mempool_alloc(struct Mempool *mp);

#endif
