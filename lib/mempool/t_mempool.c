#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include "wiremesh/mempool.h"

int main()
{
    int num_of_elements = 1000000;

    clock_t start, end;
    double cpu_time_used;

    struct Mempool *mp = wm_mempool_create(num_of_elements, sizeof(int));

    // with
    start = clock();

    if(mp != NULL)
    {
        for(int i = 0; i < num_of_elements; ++i)
        {
            int *p_i = (int *)wm_mempool_alloc(mp);

            *p_i = i;
        }
    }

    end = clock();

    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    printf("with: %ld %f s\n", num_of_elements*(sizeof(int) + sizeof(struct MempoolBlock*)), cpu_time_used);

    wm_mempool_destroy(mp);
    

    // without
    start = clock();

    for(int i = 0; i < num_of_elements; ++i)
    {
        int *p_i = (int *)malloc(sizeof(int));

        *p_i = i;
        free(p_i);
    }

    end = clock();

    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    printf("without: %ld %f s\n", num_of_elements*sizeof(int), cpu_time_used);

    return 0;
}
