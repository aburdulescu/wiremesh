#include "mempool.h"

#include <stdlib.h>

#include "log.h"


struct Mempool* wm_mempool_create(int num_blocks, int block_size)
{
    if(num_blocks <= 0)
    {
        log_error("num_blocks must be > 0");
        return NULL;
    }

    if(block_size <= 0)
    {
        log_error("block_size must be > 0");
        return NULL;
    }

    struct Mempool *mp = NULL;

    mp = (struct Mempool *)malloc(sizeof(struct Mempool));
    if(mp == NULL)
    {
        log_error("malloc returned NULL");
        return NULL;
    }

    mp->mem = malloc(num_blocks * (block_size + sizeof(struct MempoolBlock)));
    if(mp->mem == NULL)
    {
        log_error("malloc returned NULL");
        free(mp);
        return NULL;
    }

    mp->mem_block = NULL;

    for(int i = 0; i < num_blocks; ++i)
    {
        struct MempoolBlock *curr_block = NULL;
        curr_block = (struct MempoolBlock *)((char *)mp->mem + i * (block_size + sizeof(struct MempoolBlock)));

        curr_block->next = mp->mem_block;
        mp->mem_block = curr_block;
    }

    return mp;
}

void wm_mempool_destroy(struct Mempool *mp)
{
    if(mp != NULL)
    {
        if(mp->mem != NULL)
        {
            free(mp->mem);
        }
        mp->mem_block = NULL;
        free(mp);
    }
}

void* wm_mempool_alloc(struct Mempool *mp)
{
    if(mp->mem_block == NULL)
    {
        // should grow the pool !! not always(maybe is just empty
        // for now just return error
        return NULL;
    }

    // save the current block addr
    struct MempoolBlock *curr_block = mp->mem_block;

    // move forward the mem_block ptr
    mp->mem_block = mp->mem_block->next;

    return (void *)((char *)curr_block + sizeof(struct MempoolBlock));
}
