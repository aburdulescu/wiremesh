/*
Copyright (C) 2018  Andrei Burdulescu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "bdecode.h"

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "bstack.h"
#include "log.h"


#define BENCODE_INT_LITERAL   'i'
#define BENCODE_LIST_LITERAL  'l'
#define BENCODE_DICT_LITERAL  'd'
#define BENCODE_END_LITERAL   'e'
#define BENCODE_STR_SEPARATOR ':'

static struct BNode*
link_node(struct BNode *currentn, struct BNode *newn);

static struct BNode*
new_node(enum BType type, const buf_t *content, idx_t *idx);

static int
parse_string(const byte *content, idx_t *cidx, struct BData *bd);

static int
parse_integer(const buf_t *content, idx_t* cidx, struct BData *bd);

struct BNode*
wm_bdecode(const struct BData *bd)
{
    log_info("IN");
    if(bd == NULL)
    {
        log_error("bd is NULL\n");
        return NULL;
    }
    if(bd->data == NULL)
    {
        log_error("bd->data is NULL\n");
        return NULL;
    }
    if(*bd->data != BENCODE_DICT_LITERAL)
    {
        log_error("*bd->data is not %c\n", BENCODE_DICT_LITERAL);
        return NULL;
    }

    struct BNode *rootn = wm_bnode_new();
    rootn->data.sz = 1;
    rootn->data.data = bd->data;

    struct BNode *currentn = rootn;

    struct BStack *returnStack = wm_bstack_new();

    idx_t idx = 1;
    while(idx < bd->sz-1)
    {
        switch(bd->data[idx])
        {
        case BENCODE_INT_LITERAL:
            currentn = link_node(currentn,
                                 new_node(BTYPE_INTEGER, bd->data, &idx));
            break;
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            currentn = link_node(currentn,
                                 new_node(BTYPE_STRING, bd->data, &idx));
            break;
        case BENCODE_LIST_LITERAL:
            currentn = link_node(currentn,
                                 new_node(BTYPE_LIST, bd->data, &idx));
            wm_bstack_push(&returnStack, currentn);
            break;
        case BENCODE_DICT_LITERAL:
            currentn = link_node(currentn,
                                 new_node(BTYPE_DICTIONARY, bd->data, &idx));
            wm_bstack_push(&returnStack, currentn);
            break;
        case BENCODE_END_LITERAL:
            currentn = wm_bstack_pop(&returnStack);
            // store the size of the list/dict
            currentn->data.sz = idx - (currentn->data.data - bd->data) + 1;
            idx++;
            break;
        default:
            break;
        }
    }

    wm_bstack_delete(returnStack);
    
    return rootn;
}

static struct BNode*
new_node(enum BType type, const buf_t *content, idx_t *idx)
{
    struct BNode *newn = wm_bnode_new();
    newn->type = type;

    switch(type)
    {
    case BTYPE_INTEGER:
        if(parse_integer(content, idx, &newn->data) < 0)
            return NULL;
        break;
    case BTYPE_STRING:
        if(parse_string(content, idx, &newn->data) < 0)
            return NULL;
        break;
    case BTYPE_LIST:
    case BTYPE_DICTIONARY:
        newn->data.sz = 1;
        newn->data.data = content + *idx;
        (*idx)++;
        break;
    default:
        log_error("Unknown type %d", type);
        break;
    }

    return newn;
}

static struct BNode*
link_node(struct BNode *currentn, struct BNode *newn)
{
    switch(currentn->type)
    {
    case BTYPE_INTEGER:
    case BTYPE_STRING:
        currentn->sibling = newn;
        break;
    case BTYPE_LIST:
    case BTYPE_DICTIONARY:
        if(currentn->child == 0)
            currentn->child = newn;
        else
            currentn->sibling = newn;
        break;
    default:
        log_error("Unknown type %d", currentn->type);
        break;
    }
    currentn = newn;
    return currentn;
}

static int
parse_string(const byte *content, idx_t *cidx, struct BData *bd)
{	
    if(!content)
    {
        log_error("content empty");
        return -1;
    }
    if(!isdigit(content[*cidx]))
    {
        log_error("bad string element %x", content[*cidx]);
        return -1;
    }

    char* endptr;
    bd->sz = strtol(content + *cidx,
                    &endptr,
                    10);

    bd->data = endptr + 1;
    endptr = NULL; // get rid of temp pointer

    *cidx = bd->data - content + bd->sz;

    return 0;
}

static int
parse_integer(const buf_t *content, idx_t* cidx, struct BData *bd)
{
    if(!content)
    {
        log_error("content empty");
        return -1;
    }
    if(content[*cidx] != BENCODE_INT_LITERAL)
    {
        log_error("bad int element %x", content[*cidx]);
        return -1;
    }

    /*skip the 'i' at the start */
    ++(*cidx);

    bd->data = content + *cidx;
    bd->sz = (byte*)strchr(bd->data, BENCODE_END_LITERAL) - bd->data;

    *cidx += bd->sz + 1;

    return 0;
}
