/**
 * Copyright (c) 2017 rxi
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the MIT license. See `log.c` for details.
 */

#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <stdarg.h>

#define LOG_VERSION "0.1.0"

typedef void (*log_LockFn)(void *udata, int lock);

enum { LOG_TRACE, LOG_DEBUG, LOG_INFO, LOG_WARN, LOG_ERROR, LOG_FATAL };

#define log_general(level, ...) log_log(level, __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)

#define log_trace(...) log_general(LOG_TRACE, __VA_ARGS__)
#define log_debug(...) log_general(LOG_DEBUG, __VA_ARGS__)
#define log_info(...)  log_general(LOG_INFO, __VA_ARGS__)
#define log_warn(...)  log_general(LOG_WARN, __VA_ARGS__)
#define log_error(...) log_general(LOG_ERROR, __VA_ARGS__)
#define log_fatal(...) log_general(LOG_FATAL, __VA_ARGS__)

void log_set_udata(void *udata);
void log_set_lock(log_LockFn fn);
void log_set_fp(FILE *fp);
void log_set_level(int level);
void log_set_quiet(int enable);

void log_log(int         level,
             const char *file,
             const char *func,
             int         line,
             const char *fmt,
             ...);

#endif
