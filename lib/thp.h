#ifndef __WIREMESH_THP_H__
#define __WIREMESH_THP_H__

#include <curl/curl.h>

#include "common.h"

enum THPRequestEvent
{
    THP_REQUEST_EVENT_NONE,
    THP_REQUEST_EVENT_STARTED,
    THP_REQUEST_EVENT_STOPPED,
    THP_REQUEST_EVENT_COMPLETED
};

struct THPRequest
{
    // required
    byte             info_hash[BT_HASH_LENGTH];
    // required
    byte             peer_id[BT_HASH_LENGTH];
    // required
    int              port;
    // required
    int              uploaded;
    // required
    int              downloaded;
    // required
    int              left;
    // required
    int              compact;
    // optional
    int              no_peer_id;
    // may be left unspecified
    enum THPRequestEvent  event;
    // optional
    char            *ip;
    // optional
    int              numwant;
    // optional
    char            *key;
    // optional
    byte             trackerid[BT_HASH_LENGTH];
};

struct THPResponse
{
    char         *failure_reason;
    char         *warnning_message;
    unsigned int  interval;
    unsigned int  min_interval;
    byte          trackerid[BT_HASH_LENGTH];
    int           complete;
    int           incomplete;
    byte         *peers;        
};

#define THP_REQUEST_INIT() {{0}, {0}, 0, 0, 0, 0, 1, 0, THP_REQUEST_EVENT_NONE, NULL, 0, NULL, {0}}
#define THP_RESPONSE_INIT() {NULL, NULL, 0, 0, {0}, 0, 0, NULL}

typedef CURL thp_session_t;

void wm_thp_init();
void wm_thp_deinit();

thp_session_t* wm_thp_new_session();
void           wm_thp_delete_session(thp_session_t* session);

// return: 0 - SUCCESS, -1 - FAILURE
int wm_thp_send(thp_session_t *session,
                const char *announce_url,
                const struct THPRequest *req,
                struct THPResponse *rsp);

#endif /* __THP_H__ */
