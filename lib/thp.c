#include "thp.h"

#include <stdlib.h>
#include <string.h>

#include "bdata.h"
#include "log.h"


const char INFO_HASH_PARAM[]  = "info_hash";
const char PEER_ID_PARAM[]    = "peer_id";
const char PORT_PARAM[]       = "port";
const char UPLOADED_PARAM[]   = "uploaded";
const char DOWNLOADED_PARAM[] = "downloaded";
const char LEFT_PARAM[]       = "left";
const char COMPACT_PARAM[]    = "compact";
const char NO_PEER_ID_PARAM[] = "no_peer_id";
const char EVENT_PARAM[]      = "event";
const char IP_PARAM[]         = "ip";
const char NUMWANT_PARAM[]    = "numwant";
const char KEY_PARAM[]        = "key";
const char TRACKERID_PARAM[]  = "trackerid";

const size_t CHAR_SZ = sizeof(char);
const size_t INFO_HASH_PARAM_SZ  = sizeof(char) * strlen(INFO_HASH_PARAM);
const size_t PEER_ID_PARAM_SZ    = sizeof(char) * strlen(PEER_ID_PARAM);
const size_t PORT_PARAM_SZ       = sizeof(char) * strlen(PORT_PARAM);
const size_t UPLOADED_PARAM_SZ   = sizeof(char) * strlen(UPLOADED_PARAM);
const size_t DOWNLOADED_PARAM_SZ = sizeof(char) * strlen(DOWNLOADED_PARAM);
const size_t LEFT_PARAM_SZ       = sizeof(char) * strlen(LEFT_PARAM);
const size_t COMPACT_PARAM_SZ    = sizeof(char) * strlen(COMPACT_PARAM);
const size_t NO_PEER_ID_PARAM_SZ = sizeof(char) * strlen(NO_PEER_ID_PARAM);
const size_t EVENT_PARAM_SZ      = sizeof(char) * strlen(EVENT_PARAM);
const size_t IP_PARAM_SZ         = sizeof(char) * strlen(IP_PARAM);
const size_t NUMWANT_PARAM_SZ    = sizeof(char) * strlen(NUMWANT_PARAM);
const size_t KEY_PARAM_SZ        = sizeof(char) * strlen(KEY_PARAM);
const size_t TRACKERID_PARAM_SZ  = sizeof(char) * strlen(TRACKERID_PARAM);


// private functions prototypes
static size_t rsp_cb(char *buffer, size_t size, size_t nmemb, void *userdata);
static char* build_request_string(const char *announce_url,
                                  const struct THPRequest *req);
static char* urlencode(const byte *data);
static int get_strlen_of_int(int input);
static int int_to_string(int input, char *output, int len);
static void req_str_add_param(char *req_str,
                              const char *param_name, const int param_name_sz,
                              const char *param_value, const int param_value_sz);

// public functions
void wm_thp_init()
{
    curl_global_init(CURL_GLOBAL_ALL);
}

void wm_thp_deinit()
{
    curl_global_cleanup();
}

thp_session_t* wm_thp_new_session()
{
    return curl_easy_init();
}

void wm_thp_delete_session(thp_session_t* session)
{
    curl_easy_cleanup(session);
}

int wm_thp_send(thp_session_t *session,
                const char *announce_url,
                const struct THPRequest *req,
                struct THPResponse *rsp)
{
    CURLcode res = CURLE_OK;
    struct BData rsp_data = {NULL, 0};

    char *url = build_request_string(announce_url, req);

    printf("req query string: %s\n", url);

    /* curl_easy_setopt(session, CURLOPT_URL, url); */
    /* curl_easy_setopt(session, CURLOPT_WRITEFUNCTION, rsp_cb); */
    /* curl_easy_setopt(session, CURLOPT_WRITEDATA, (void*)&rsp_data); */
    /* res = curl_easy_perform(session); */

    free(url);

    if(res != CURLE_OK)
    {
        log_error("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        return -1;
    }
    else
    {
        // decode response
        // ...
        // free(rsp_data.data)
    }
    return 0;
}

// private functions
static size_t rsp_cb(char *buffer, size_t size, size_t nmemb, void *userdata)
{
    size_t realsize = size * nmemb;

    struct BData *rsp = (struct BData*)userdata;

    if(rsp->data == NULL)
        rsp->data = (byte *)malloc(realsize + 1);
    else
        rsp->data = (byte *)realloc(rsp->data, rsp->sz + realsize + 1);

    if(rsp->data == NULL)
    {
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }
 
    memcpy(&(rsp->data[rsp->sz]), buffer, realsize);
    rsp->sz += realsize;
    rsp->data[rsp->sz] = 0;
 
    return realsize;
}

static char* build_request_string(const char *announce_url,
                                  const struct THPRequest *req)
{
    // ============check request parameters==============
    if(strlen(req->info_hash) == 0)
    {
        log_info("info_hash is empty");
        return NULL;
    }
    if(strlen(req->peer_id) == 0)
    {
        log_info("peer_id is empty");
        return NULL;
    }
    if(req->port == 0)
    {
        log_info("port is 0");
        return NULL;
    }

    // ==========convert data===============
    
    // url encode parameters that contain binary data
    char *info_hash_conv = urlencode(req->info_hash);
    if(info_hash_conv == NULL)
    {
        log_error("info_hash url encoding failed");
        return NULL;
    }
    const int info_hash_conv_sz = sizeof(char)*strlen(info_hash_conv);
    
    /* char *peer_id_conv = urlencode(req->peer_id); */
    /* if(peer_id_conv == NULL) */
    /* { */
    /*     log_error("peer_id url encoding failed"); */
    /*     return NULL; */
    /* } */
    const int peer_id_conv_sz = BT_HASH_LENGTH;

    const int port_conv_sz = get_strlen_of_int(req->port);
    char port_conv[port_conv_sz+1];
    int_to_string(req->port, port_conv, port_conv_sz);

    const int uploaded_conv_sz = get_strlen_of_int(req->uploaded);
    char uploaded_conv[uploaded_conv_sz+1];
    int_to_string(req->uploaded, uploaded_conv, uploaded_conv_sz);

    const int downloaded_conv_sz = get_strlen_of_int(req->downloaded);
    char downloaded_conv[downloaded_conv_sz+1];
    int_to_string(req->downloaded, downloaded_conv, downloaded_conv_sz);

    const int left_conv_sz = get_strlen_of_int(req->left);
    char left_conv[left_conv_sz+1];
    int_to_string(req->left, left_conv, left_conv_sz);

    const int compact_conv_sz = get_strlen_of_int(req->compact);
    char compact_conv[compact_conv_sz+1];
    int_to_string(req->compact, compact_conv, compact_conv_sz);

    // ============build the request string===========
    // request string has the following structure:
    // "announce_url?param0=val0&param1=val1 ... paramn=valn"

    // 1)determine the size of the req_str
    
    const size_t ANNOUNCE_URL_SZ = sizeof(char) * strlen(announce_url);

    // +1 for each metacharacter('?', '=', '&')
    size_t req_str_sz =
        ANNOUNCE_URL_SZ
        + CHAR_SZ // ?
        + INFO_HASH_PARAM_SZ
        + CHAR_SZ // =
        + info_hash_conv_sz
        + CHAR_SZ // &
        + PEER_ID_PARAM_SZ
        + CHAR_SZ // =
        + peer_id_conv_sz
        + CHAR_SZ // &
        + PORT_PARAM_SZ
        + CHAR_SZ // =
        + port_conv_sz
        + CHAR_SZ // &
        + UPLOADED_PARAM_SZ
        + CHAR_SZ // =
        + uploaded_conv_sz
        + CHAR_SZ // &
        + DOWNLOADED_PARAM_SZ
        + CHAR_SZ // =
        + downloaded_conv_sz
        + CHAR_SZ // &
        + LEFT_PARAM_SZ
        + CHAR_SZ // =
        + left_conv_sz
        + CHAR_SZ // &
        + COMPACT_PARAM_SZ
        + CHAR_SZ // =
        + compact_conv_sz;

    char *req_str = (char *)malloc(sizeof(char)*req_str_sz);

    memset(req_str, 0, req_str_sz);

    memcpy(req_str, announce_url, ANNOUNCE_URL_SZ);
    strncat(req_str, "?", CHAR_SZ);

    req_str_add_param(req_str,
                      INFO_HASH_PARAM, INFO_HASH_PARAM_SZ,
                      info_hash_conv, info_hash_conv_sz);
    free(info_hash_conv);
    
    strncat(req_str, "&", CHAR_SZ);

    req_str_add_param(req_str,
                      PEER_ID_PARAM, PEER_ID_PARAM_SZ,
                      req->peer_id, peer_id_conv_sz);

    strncat(req_str, "&", CHAR_SZ);

    req_str_add_param(req_str,
                      PORT_PARAM, PORT_PARAM_SZ,
                      port_conv, port_conv_sz);

    strncat(req_str, "&", CHAR_SZ);

    req_str_add_param(req_str,
                      UPLOADED_PARAM, UPLOADED_PARAM_SZ,
                      uploaded_conv, uploaded_conv_sz);

    strncat(req_str, "&", CHAR_SZ);

    req_str_add_param(req_str,
                      DOWNLOADED_PARAM, DOWNLOADED_PARAM_SZ,
                      downloaded_conv, downloaded_conv_sz);

    strncat(req_str, "&", CHAR_SZ);

    req_str_add_param(req_str,
                      LEFT_PARAM, LEFT_PARAM_SZ,
                      left_conv, left_conv_sz);

    strncat(req_str, "&", CHAR_SZ);

    req_str_add_param(req_str,
                      COMPACT_PARAM, COMPACT_PARAM_SZ,
                      compact_conv, compact_conv_sz);

    // ==========now add optional params===================

    int is_no_peer_id_present = !(req->no_peer_id == 0);
    int is_event_present      = !(req->event == THP_REQUEST_EVENT_NONE);
    int is_ip_present         = !(req->ip == NULL);
    int is_numwant_present    = !(req->numwant == 0);
    int is_key_present        = !(req->key == NULL);
    int is_trackerid_present  = !(strlen(req->trackerid) == 0);

    if(is_no_peer_id_present)
    {
        const int no_peer_id_conv_sz = get_strlen_of_int(req->no_peer_id);
        char no_peer_id_conv[no_peer_id_conv_sz+1];
        int_to_string(req->no_peer_id, no_peer_id_conv, no_peer_id_conv_sz);

        int tmp_sz = CHAR_SZ + NO_PEER_ID_PARAM_SZ + CHAR_SZ + no_peer_id_conv_sz;

        req_str = (char *)realloc(req_str, tmp_sz);

        strncat(req_str, "&", CHAR_SZ);
        req_str_add_param(req_str,
                          NO_PEER_ID_PARAM, NO_PEER_ID_PARAM_SZ,
                          no_peer_id_conv, no_peer_id_conv_sz);
    }
    if(is_event_present)
    {
        const int event_conv_sz = get_strlen_of_int(req->event);
        char event_conv[event_conv_sz+1];
        int_to_string(req->event, event_conv, event_conv_sz);

        int tmp_sz = CHAR_SZ + EVENT_PARAM_SZ + CHAR_SZ + event_conv_sz;

        req_str = (char *)realloc(req_str, tmp_sz);

        strncat(req_str, "&", CHAR_SZ);
        req_str_add_param(req_str,
                          EVENT_PARAM, EVENT_PARAM_SZ,
                          event_conv, event_conv_sz);
    }
    if(is_ip_present)
    {
        const int ip_sz = strlen(req->ip);

        int tmp_sz = CHAR_SZ + IP_PARAM_SZ + CHAR_SZ + ip_sz;

        req_str = (char *)realloc(req_str, tmp_sz);

        strncat(req_str, "&", CHAR_SZ);
        req_str_add_param(req_str,
                          IP_PARAM, IP_PARAM_SZ,
                          req->ip, ip_sz);
    }
    if(is_numwant_present)
    {
        const int numwant_conv_sz = get_strlen_of_int(req->numwant);
        char numwant_conv[numwant_conv_sz+1];
        int_to_string(req->numwant, numwant_conv, numwant_conv_sz);

        int tmp_sz = CHAR_SZ + NUMWANT_PARAM_SZ + CHAR_SZ + numwant_conv_sz;

        req_str = (char *)realloc(req_str, tmp_sz);

        strncat(req_str, "&", CHAR_SZ);
        req_str_add_param(req_str,
                          NUMWANT_PARAM, NUMWANT_PARAM_SZ,
                          numwant_conv, numwant_conv_sz);
    }
    if(is_key_present)
    {
        const int key_sz = strlen(req->key);

        int tmp_sz = CHAR_SZ + KEY_PARAM_SZ + CHAR_SZ + key_sz;

        req_str = (char *)realloc(req_str, tmp_sz);

        strncat(req_str, "&", CHAR_SZ);
        req_str_add_param(req_str,
                          KEY_PARAM, KEY_PARAM_SZ,
                          req->key, key_sz);
    }
    if(is_trackerid_present)
    {
        char *trackerid_conv = urlencode(req->trackerid);
        if(trackerid_conv == NULL)
        {
            log_error("trackerid url encoding failed");
            return NULL;
        }
        const int trackerid_conv_sz = sizeof(char)*strlen(trackerid_conv);

        int tmp_sz = CHAR_SZ + TRACKERID_PARAM_SZ + CHAR_SZ + trackerid_conv_sz;

        req_str = (char *)realloc(req_str, tmp_sz);

        strncat(req_str, "&", CHAR_SZ);
        req_str_add_param(req_str,
                          TRACKERID_PARAM, TRACKERID_PARAM_SZ,
                          trackerid_conv, trackerid_conv_sz);
        free(trackerid_conv);
    }

    return req_str;
}

// returned string must be free'd  by caller
static char* urlencode(const byte *data)
{
    char *urlstr = NULL;
    
    CURL *curl = curl_easy_init();
    if(curl != NULL)
    {
        urlstr = curl_easy_escape(curl, data, sizeof(data));
        if(urlstr == NULL)
        {
            log_error("curl_easy_escape returned NULL");
        }
    }
    else
    {
        log_error("curl_easy_init returned NULL");
    }

    curl_easy_cleanup(curl);

    return urlstr;
}

static int get_strlen_of_int(int input)
{
    return snprintf( NULL, 0, "%d", input);
}

static int int_to_string(int input, char *output, int len)
{
    return snprintf(output, len+1, "%d", input);
}

static void req_str_add_param(char *req_str,
                              const char *param_name, const int param_name_sz,
                              const char *param_value, const int param_value_sz)
{
    strncat(req_str, param_name, param_name_sz);
    strncat(req_str, "=", CHAR_SZ);
    strncat(req_str, param_value, param_value_sz);
}
