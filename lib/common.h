/*
Copyright (C) 2018  Andrei Burdulescu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __WIREMESH_COMMON_H__
#define __WIREMESH_COMMON_H__

#include <stdio.h>

#define BT_HASH_LENGTH 20

typedef unsigned char byte;     // TODO: rename to 'byte_t'
typedef char          buf_t;    // TODO: remove and replace with byte
typedef unsigned int  idx_t;

#endif
