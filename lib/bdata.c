/*
Copyright (C) 2018  Andrei Burdulescu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "bdata.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "log.h"

struct BData*
wm_bdata_new(byte *data, int sz)
{
    struct BData *tmp;

    tmp = (struct BData*)malloc(sizeof(struct BData));

    if(!tmp)
    {
        log_fatal("Something went wrong, malloc returned NULL");
        return NULL;
    }

    tmp->data = data;
    tmp->sz = sz;

    return tmp;
}

void
wm_bdata_delete(struct BData *bd)
{
    log_info("IN");
    if(bd != NULL)
    {
        if(bd->data != NULL)
            free(bd->data);
        free(bd);
    }
}

char*
wm_bdata_to_string(struct BData in)
{
    char *out = (char*)malloc(sizeof(char)*in.sz + 1);
    if(!out)
    {
        log_fatal("Something went wrong, malloc returned NULL");
        return 0;
    }
    
    memcpy(out, in.data, in.sz);

    out[in.sz] = 0;

    return out;
}
