/*
Copyright (C) 2018  Andrei Burdulescu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __WIREMESH_BDATA_H__
#define __WIREMESH_BDATA_H__

#include "common.h"

struct BData
{
    byte* data;
    int   sz;
};

struct BData* wm_bdata_new(byte *data, int sz);
void          wm_bdata_delete(struct BData* bd);
char*         wm_bdata_to_string(struct BData in);

#endif
