/*
Copyright (C) 2018  Andrei Burdulescu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "bfile.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "log.h"


static int
open_file(const char *path, const char *mode, FILE **fp);

static size_t
get_file_size(FILE *fp);

int
wm_bfile_read(const char* path, struct BData *bd)
{
    log_info("IN");
    
    FILE* fp = 0;
 
    if(open_file(path, "rb", &fp) < 0)
    {
        return -1;
    }

    bd->sz = get_file_size(fp);
    bd->data = (byte*)malloc(sizeof(byte) * bd->sz);

    fread(bd->data, bd->sz, 1, fp);

    fclose(fp);

    return 0;
}

void
wm_bfile_print(const struct BData *bd)
{
    if(!bd)
        return;

    printf("\nFile content:\n");
    for(int i = 0; i < bd->sz; ++i)
    {
        printf("%c", bd->data[i]);
    }
    printf("\n");  
}

static int
open_file(const char *path, const char *mode, FILE **fp)
{
    *fp = fopen(path, mode);
    if(!*fp)
    {
        log_error("Error while opening the file: %s", strerror(errno));
        return -1;
    }
    return 0;
}

static size_t
get_file_size(FILE *fp)
{
    size_t size = 0;
 
    fseek(fp, 0, SEEK_END);
    size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    return size;
}
