/*
Copyright (C) 2018  Andrei Burdulescu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "bnode.h"

#include <stdlib.h>
#include <string.h>

#include "bstack.h"
#include "log.h"


static void
print_priv(struct BNode *root);


struct BNode*
wm_bnode_new()
{
    struct BNode *node;

    node = (struct BNode *)malloc(sizeof(struct BNode));

    if(!node)
    {
        log_fatal("malloc returned NULL");
        return NULL;
    }
    
    node->type = BTYPE_DICTIONARY;
    node->data.data = NULL;
    node->data.sz = 0;
    node->child = NULL;
    node->sibling = NULL;

    return node;
}

/* Given a binary tree, print its nodes in inorder*/
void
wm_bnode_print(struct BNode *root)
{
    printf("File content:\n");
    printf("[node type, node data, node child, node sibling]\n");
    print_priv(root);
}

struct BData*
old_bnode_getBnodeData(const struct BNode *n)
{
    if(!n)
        return NULL;

    struct BData *d = wm_bdata_new(n->data.data, n->data.sz);

    struct BStack *returnStack = wm_bstack_new();
    wm_bstack_push(&returnStack, (void*)n);

    struct BNode *nextn = NULL;

    while(!wm_bstack_is_empty(returnStack))
    {
        nextn = wm_bstack_pop(&returnStack);
        d->sz += nextn->data.sz;

        if(nextn->child)
            wm_bstack_push(&returnStack, nextn->child);
        if(nextn->sibling)
            wm_bstack_push(&returnStack, nextn->sibling);
        nextn = NULL;
    }

    wm_bstack_delete(returnStack);

    return d;
}

struct BNode*
wm_bnode_get_bnode_by_key(const struct BNode *root, const char *key)
{
    if(root->type != BTYPE_DICTIONARY)
    {
        log_error("node should be a dictionary");            
        return NULL;
    }

    struct BNode *p = root->child;
    struct BNode *result = NULL;
    buf_t *temp = NULL;
    
    while (p)
    {
        if(p->type != BTYPE_STRING)
        {
            log_error("wrong bencode format: node should be a string %s", p->data.data);            
            break;
        }
        else
        {
            temp = wm_bdata_to_string(p->data);
            if(strcmp(key, temp) == 0)
            {
                result = p->sibling;
                break;
            }
            free(temp);
            temp = NULL;

            p = p->sibling->sibling;
        }
    }

    free(temp);

    return result;
}

void
wm_bnode_delete(struct BNode *bn)
{
    if(bn == NULL)
        return;
    
    struct BStack *st = wm_bstack_new();
    wm_bstack_push(&st, bn);

    struct BNode *n = NULL;

    while(!wm_bstack_is_empty(st))
    {
        n = wm_bstack_pop(&st);

        if(n->child != NULL)
        {
            wm_bstack_push(&st, n->child);
        }
        if(n->sibling != NULL)
        {
            wm_bstack_push(&st, n->sibling);
        }

        free(n);
    }

    wm_bstack_delete(st);
}

static void
print_priv(struct BNode *root)
{
    if (root == NULL)
        return;

    buf_t *out = wm_bdata_to_string(root->data);

    printf("[%d, %s, %d]\n",
           root->type,
           out,
           root->data.sz);
    free(out);

    print_priv(root->child);
    print_priv(root->sibling);
}
