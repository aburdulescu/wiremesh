#include "net.h"

#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

int create_socket(const struct NetSocketParams *s,
                  const struct NetAddress *addr,
                  struct NetRawAddress *raw_addr);

int wm_net_new(const struct NetSocketParams *s,
               const struct NetAddress *addr,
               struct NetRawAddress *raw_addr)
{
    int sfd = -1;

    sfd = create_socket(s, addr, raw_addr);

    return sfd;
}

int wm_net_ntop(char *ip, int ip_sz, const struct NetRawAddress *addr)
{
    switch(addr->type)
    {
    case NET_RAW_ADDRESS_TYPE_IPV4:
    {
        if(ip_sz < INET_ADDRSTRLEN)
            return -1;
        if(inet_ntop(AF_INET, &addr->data, ip, ip_sz) == NULL)
        {
        }
        break;
    }
    case NET_RAW_ADDRESS_TYPE_IPV6:
    {
        if(ip_sz < INET6_ADDRSTRLEN)
            return -1;
        if(inet_ntop(AF_INET6, &addr->data, ip, ip_sz) == NULL)
        {
        }
        break;
    }
    default:
    {
        return -1;
    }
    }
    return 0;
}

void wm_net_destroy(int sfd)
{
    close(sfd);
}

int create_socket(const struct NetSocketParams *s,
                  const struct NetAddress *addr,
                  struct NetRawAddress *raw_addr)
{
    int sfd = -1;

    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = s->domain;
    hints.ai_socktype = s->type;
    hints.ai_protocol = s->protocol;
    hints.ai_flags = s->flags;

    struct addrinfo *res = NULL;

    int rc = getaddrinfo(addr->node, addr->service, &hints, &res);
    if(rc < 0)
    {
        printf("%s:%s - getaddrinfo error: %s\n", __FILE__, __FUNCTION__, gai_strerror(rc));
        return -1;
    }

    struct addrinfo *p;
    for(p = res; p != NULL; p = p->ai_next)
    {
        if((sfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
            continue;
        if(addr->node != NULL)
        {
            // client
            if(connect(sfd, p->ai_addr, p->ai_addrlen) == -1)
            {
                close(sfd);
                continue;
            }
        }
        else
        {
            //server
            int yes = 1;
            if(setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
            {
                close(sfd);
                continue;
            }
            if(bind(sfd, p->ai_addr, p->ai_addrlen) == -1)
            {
                close(sfd);
                continue;
            }
        }

        break;
    }

    if(p == NULL)
    {
        printf("%s:%s - failed to connect\n", __FILE__, __FUNCTION__);
        freeaddrinfo(res);
        return -1;
    }

    if(raw_addr != NULL)
    {
        memset(raw_addr, 0, sizeof(struct NetRawAddress));
        memcpy(&raw_addr->data, p->ai_addr, sizeof(struct sockaddr));
        if(p->ai_family == AF_INET)
            raw_addr->type = NET_RAW_ADDRESS_TYPE_IPV4;
        else if(p->ai_family == AF_INET6)
            raw_addr->type = NET_RAW_ADDRESS_TYPE_IPV6;
        else
            return -1;
    }

    freeaddrinfo(res);

    return sfd;
}
