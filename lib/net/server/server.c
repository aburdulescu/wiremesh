#include <stdio.h>
#include <string.h>

#include "wiremesh/net.h"

int main()
{
    struct NetSocketParams sock_params = NET_SOCKET_PARAMS_INIT;
    struct NetAddress addr = {NULL, "65535"};
    struct NetRawAddress raw_addr;

    int sfd = wm_net_new(&sock_params, &addr, &raw_addr);
    if(sfd < 0)
    {
        printf("error while creating client\n");
        return 1;
    }
    if (listen(sfd, 10) == -1) {
        perror("listen");
        return 1;
    }

    printf("server: waiting for connections...\n");

    struct sockaddr_storage their_addr;
    socklen_t sin_size = sizeof(*(struct sockaddr_in *)&their_addr);
    int new_sfd = accept(sfd, (struct sockaddr *)&their_addr, &sin_size);
    if (new_sfd == -1) {
        perror("accept");
        return -1;
    }

    printf("got connection\n");
    close(sfd);
    char msg[255] = {0};
    if(recv(new_sfd, msg, strlen(msg), 0) == -1)
    {
        return 1;
    }
    printf("received: %s\n", msg);
    close(new_sfd);

    return 0;
}
