#ifndef __WIREMESH_NET_H__
#define __WIREMESH_NET_H__

#include <netinet/in.h>
#include <unistd.h>


struct NetSocketParams
{
    int domain; // AF_INET, AF_INET6 etc.
    int type; // SOCK_STREAM, SOCK_DGRAM etc.
    int protocol;
    int flags;
};

struct NetAddress
{
    char *node; // "192.168.0.1" or "www.example.com"
    char *service; // 1024 < port <= 65535 or "tcp", "udp" etc.
};

enum NetRawAddressType
{
    NET_RAW_ADDRESS_TYPE_UNKNOWN,
    NET_RAW_ADDRESS_TYPE_IPV4,
    NET_RAW_ADDRESS_TYPE_IPV6,
};

struct NetRawAddress
{
    struct sockaddr_storage data;
    enum NetRawAddressType type;
};

#define NET_SOCKET_PARAMS_INIT {AF_INET, SOCK_STREAM, 0, 0}
#define NET_ADDRESS_INIT {NULL, NULL}

int wm_net_new(const struct NetSocketParams *s,
               const struct NetAddress *addr,
               struct NetRawAddress *raw_addr);

int wm_net_ntop(char *ip, int ip_sz, const struct NetRawAddress *addr);

void wm_net_destroy(int sfd);

#endif // __WIREMESH_NET_H__
