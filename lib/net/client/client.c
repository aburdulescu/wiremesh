#include <stdio.h>
#include <string.h>

#include "wiremesh/net.h"

int main()
{
    struct NetSocketParams sock_params = NET_SOCKET_PARAMS_INIT;
    struct NetAddress addr = {"localhost", "65535"};

    int sfd = wm_net_new(&sock_params, &addr, NULL);
    if(sfd < 0)
    {
        printf("error while creating client\n");
        return 1;
    }

    char msg[] = "hi";
    if(send(sfd, msg, strlen(msg), 0) == -1)
    {
        return 1;
    }

    return 0;
}
