#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <curl/curl.h>

struct Rsp
{
    char *d;
    int sz;
};


size_t rsp_cb(char *ptr, size_t size, size_t nmemb, void *userdata)
{
    size_t realsize = size * nmemb;

    struct Rsp *rsp = (struct Rsp*)userdata;
 
    rsp->d = realloc(rsp->d, rsp->sz + realsize + 1);
    if(rsp->d == NULL)
    {
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }
 
    memcpy(&(rsp->d[rsp->sz]), ptr, realsize);
    rsp->sz += realsize;
    rsp->d[rsp->sz] = 0;
 
    return realsize;
}

void send_req(struct Rsp *rsp)
{
    curl_global_init(CURL_GLOBAL_ALL);
    CURL *curl = curl_easy_init();
    if(curl) {
        CURLcode res;
        curl_easy_setopt(curl, CURLOPT_URL, "https://www.google.ro");
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, rsp_cb);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, rsp);
        res = curl_easy_perform(curl);
        if(res != CURLE_OK)
        {
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
        }
        else
        {
            /*
             * Now, our chunk.memory points to a memory block that is chunk.size
             * bytes big and contains the remote file.
             *
             * Do something nice with it!
             */ 
 
            printf("%lu bytes retrieved\n", (long)rsp->sz);
        }

        char *str = curl_easy_escape(curl, "some data", 9);
        curl_easy_cleanup(curl);

        printf(str);
        
        free(str);
    }
    curl_global_cleanup();
}

int main()
{
    struct Rsp rsp = {NULL, 0};
    
    send_req(&rsp);

    if(rsp.d == NULL)
    {
        printf("rsp.d == NULL\n");
        return 1;
    }

    printf("%d bytes received\n", rsp.sz);

    free(rsp.d);
    
    return 0;
}
