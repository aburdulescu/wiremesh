import web

urls = (
    '/', 'Index',
    '/about', 'About'
)

render = web.template.render('templates', globals())
app = web.application(urls, globals())


class Index:
    def GET(self, name='World'):
        return render.index(name)


class About:
    def GET(self):
        return render.about()


if __name__ == "__main__":
    app.run()
