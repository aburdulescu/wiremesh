#!/bin/bash

if [[ -z $1 ]]
then
    export WM_SUBNET=172.255.0.0/16
    echo "Subnet not provided => using default subnet: $WM_SUBNET"
else
    export WM_SUBNET=$1
fi

export WM_NETWORK_NAME=wm

docker network rm $WM_NETWORK_NAME
docker network create --subnet $WM_SUBNET $WM_NETWORK_NAME \
&& docker network inspect $WM_NETWORK_NAME

export WM_TR_IP=$(echo $WM_SUBNET |cut -d "/" -f 1 | cut -d "." -f 1,2,3)".10"
