#!/bin/bash

if [[ $# -ne 1 ]]
then
    echo "Subnet not provided."
    exit 1
fi

source setup.sh $1

tmux new-window
tmux rename-window "wm-sim"

tmux split-window "docker run --network $WM_NETWORK_NAME --ip $WM_TR_IP -it aburdulescu/wiremesh-tr:latest"

tmux split-window "docker run --network $WM_NETWORK_NAME -it aburdulescu/wiremesh-cli:latest"
