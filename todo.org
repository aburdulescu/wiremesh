* lib [0/2]
- [ ] fix coding style issues
- [ ] use log library instead of printf, and put relevant logs in all functions
* cli [1/4]
- [ ] add thp support
- [ ] fix coding style issues
- [ ] use log library instead of printf, and put relevant logs in all functions
* test
** DONE base os docker image for tr and cli
** DONE cli docker image
** DONE tracker docker image
** DONE create custom network tracker <-> clients communication
** TODO move all the above into 'sim' dir
** TODO add to the cli image a fully featured bt client(transmission)
** TODO start running simulations to test the environment
* other [0/0]


