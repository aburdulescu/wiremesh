# WireMesh C coding style

This documents describes the C coding style used by the WireMesh project.

## Table of contents
1. [Line width](#line_width)
2. [Indentation](#indentation)
3. [Braces](#braces)
4. [If conditions](#if_conditions)
5. [Functions](#functions)
6. [Conditions](#conditions)
7. [Whitespace](#whitespace)
7. [Switch statement](#switch)
8. [Typedefs](#typedefs)
9. [Headers](#headers)
10. [Naming](#naming)
11. [Things not specified by this document](#not_specified)

### Line width <a name="line_width"></a>

Keep the lines 80 characters or less.
If the line exceeds 80 characters, you should try to break the line.
If breaking the line does not work, please have in mind the following:
- if you have too many levels of indentation, consider restructuring your code;
- if the chosen variables/functions names are too long, consider renaming them;
- if a function has a long list of parameters, make sure that all are necessary.
If yes, try creating a struct with all/some of the parameters and pass the struct to the function instead.

### Indentation <a name="indentation"></a>

Indent using spaces with a length of 4 characters.

### Braces <a name="braces"></a>
- Braces go on a line by themselves with no indentation for the braces.
```
if(cond)
{
    // do things
}
else
{
    // do other things
}
```

- Use braces for single-statement blocks.
```
if(cond)
{
    // single statement
}
else
{
    // statement 0
    // ...
    // statment n
}
```
### If conditions <a name="if_conditions"></a>

If the condition is composed of multiple conditions, put each condition on its line.
The separation of the conditions is left to the developer.
The operator between condition has to be put at the begining of the line.
Each condition line is is indented to the first condition indentation level.
```
if (condition1
    || (condition2 && condition3)
    || condition4
    || (condition5 && (condition6 || condition7)))
{
	a_single_statement ();
}
```
Such long conditions are usually hard to understand and should not be put in a 'if'.
A good practice is to set the condition to a boolean variable, with a good name for that variable.
Another way is to move the long condition to a function if the condition is used in many places.

### Functions <a name="functions"></a>

Function return type should be place on a separate line than the function name and arguments.
```
static void
my_function(int argument)
{
    // do stuff
}
```
If the function has a lot of arguments and breaks the line width rule, follow the recommandations given at that rule.
If you break the line and place each argument on its separate line, indent the line to the first line.

Put the '*' on the side of the parameter name for pointer parameters:
```
void
my_function (type_t  *a_pointer,
             type_t   **double_pointer)
{
    // stuff
}
```

Functions should be short and to one thing.
If a function is becoming to big, break it into smaller functions with decriptive names.

### Conditions <a name="conditions"></a>

- Don't check boolean value for equality.
Use implicit comparisons(the code can be read more like conventional english this way).
```
if(isEmpty)
{
    // do stuff
}

if(!exists)
{
    // do stuff
}
```
- C uses 0 for many purposes(numeric value, NULL pointer, end of string, boolean false).
For boolean variables, an implicit comparison is correct because it's a logical expression.
But for other types, implicit comparisons are not appropriate. So a explict comparison is better:
```
if(pointer == NULL)
{
    // do stuff
}

if(number == 0)
{
    // do stuff
}

if(str != NULL && *str != '\0')
{
    // do stuff
}
```
Also, don't use 0 for NULL and end of string.

### Whitespace <a name="whitespace"></a>
- Don't put a space between if/for/while/etc. and the opening paranthesis.

Valid:
```
if(cond)
{
    // stuff
}
```

Invalid:
```
if (cond)
{
    // stuff
}
```

- Use whitespace to align struct members and function parameters.

Valid:
```
struct some_struct
{
    int   a; // a int
    float b; // a flaot
};
```

Invalid:
```
struct some_struct
{
    int a; // a int
    float b; // a flaot
};
```


Valid:
```
int
my_func(int   arg0, // arg0
        float arg1 // arg1)
{
    // stuff
}
```

Invalid:
```
int
my_func(int arg0, // arg0
        float arg1 // arg1)
{
    // stuff
}
```

In Emacs this can be done by marking the wanted region and entering ```M-x align```.

### Switch statement <a name="switch"></a>
Each ```case``` statement has to be on the same indentation level as the ```switch``` statement.
The ```case``` block and ```break``` statement has to be on the next indentation level.
Each ```case``` block(including the ```break```) has to be enclosed in braces.

Valid:
```
switch(var)
{
case v0:
{
    // stuff
    break;
}
case v1:
{
    // stuff
    break;
}
default:
{
    // stuff
    break;
}
}
```

Invalid:
```
switch(var)
{
case v0:
    // stuff
break;
case v1: // stuff; break;
default:
{
    // stuff
    break;
}
}
```

### Typedefs <a name="typedefs"></a>
Don't use typedef for pointers, structs, enums etc.
It does not improve readability, quite the oposite.

Lets say we have the types:
```
struct my_struct
{
    int a;
    int b;
};

enum my_enum
{
    ENUM_LITERAL0,
    ENUM_LITERAL1
};

typedef struct my_struct my_s_t;
typedef enum my_enum my_e_t;
```

Which is easier to understand?

This:
```
// what type will 'a' or 'b' have?
// you will have to search for the typedef definition to find out
my_s_t a;
my_e_t b; 
```

Or this:
```
// here it is clear that 'a' will be of type 'my_struct' and 'b' of type 'my_enum'
struct my_struct a;
enum my_enum b;
```

### Headers <a name="headers"></a>
All headers must have inclusion guards.
Inclusion guard macros have to look like this:
```
#ifndef __MYPROJECT_MODULENAME_HEADERFILENAME_H__
#define __MYPROJECT_MODULENAME_HEADERFILENAME_H__

// stuff

#endif
```

E.g.(for bdecode.h file from wiremesh bencode module):
```
#ifndef __WIREMESH_BENCODE_BDECODE_H__
#define __WIREMESH_BENCODE_BDECODE_H__

// stuff

#endif

```

### Naming <a name="naming"></a>
#### Files
Files should have short suggestive names.

#### Functions
All public functions in a file must be prefixed with the filename and the project prefix('wm').
Function name(including prefix) is lowercase with underscores separating different words.
E.g.:
- file bdecode.c:
```
void
wm_bdecode_some_random_public_function()
{
}

static void
some_random_private_function()
{
}
```
#### Variables
All variables should be lowercase with underscores separating words.
Global variables should have a prefix('g').
```
int g_int_var;

void
foo(int func_param)
{
    func_param *= g_int_var;
}
```
#### Custom types(structs, unions, enums, macros)
All custom types are CamelCase with the first letter uppercase.
Except macros/constants which are all uppercase with underscores separating different words.
Enum literals follow macros/constants nameing convention and are prefixed with the enum name.

E.g.:
- file bnode.h:
```
struct BNode
{
    // stuff
}

enum BType
{
    BTYPE_INTEGER,
    BTYPE_STRING
}

#define MAX_LENGTH 10
```

### Things not specified by this document <a name="not_specified"></a>
If there is anything that is not explicity specified by this file use your common sense and the examples provided in this document to
decide what to do.
