/*
Copyright (C) 2018  Andrei Burdulescu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <openssl/sha.h>

#include "wiremesh/bdecode.h"
#include "wiremesh/bfile.h"
#include "wiremesh/log.h"
#include "wiremesh/thp.h"


typedef int (*cmd_callback_t)(const char *str);

struct cmd
{
    cmd_callback_t  cmd_cb;
    char           *cmd_str;
};

static void get_rand_string(char *output, int sz);

int cmd_help(const char *progname);
int cmd_infohash(const char *filepath);
int cmd_pieces(const char *filepath);
int cmd_info(const char *filepath);
int cmd_print(const char *filepath);
int cmd_peers(const char *filepath);

const struct cmd g_commands[] = 
{
    {cmd_help, "help"},
    {cmd_infohash, "infohash"},
    {cmd_pieces, "pieces"},
    {cmd_info, "info"},
    {cmd_print, "print"},
    {cmd_peers, "peers"}
};

struct args
{
    cmd_callback_t cmd_cb;
    char *filepath;
};

const int G_COMMANDS_SIZE = sizeof(g_commands);

int parse_args(int argc, char* argv[], struct args *args);

int main(int argc, char* argv[])
{
    struct args args;
    if(parse_args(argc, argv, &args) < 0)
        return 1;

    FILE *logfile_fp = fopen("wiremesh-cli.log", "wb");
    if(logfile_fp != NULL)
    {
        log_set_fp(logfile_fp);
        log_set_quiet(1);
    }

    args.cmd_cb(args.filepath);

    fclose(logfile_fp);
    
    return 0;
}

int parse_args(int argc, char* argv[], struct args *args)
{
    char *PROG_NAME = argv[0];
    if(argc == 1)
    {
        printf("No arguments given.\nEnter %s help for help.\n",
               PROG_NAME);
        return -1;
    }

    if(argc == 2
       && strcmp(argv[1], "help") == 0)
    {
        args->cmd_cb = cmd_help;
        args->filepath = PROG_NAME;
        return 0;
    }

    int NO_ARGS = 3;
    if(argc != NO_ARGS)
    {
        printf("%s needs %d arguments.\nEnter %s help for help.\n",
               PROG_NAME, NO_ARGS-1, PROG_NAME);
        return -1;
    }

    char *CMD = argv[1];
    char *FILEPATH = argv[2];

    int i;
    for(i = 0; strcmp(g_commands[i].cmd_str, CMD) != 0 && i < G_COMMANDS_SIZE; ++i);

    if(i == G_COMMANDS_SIZE)
    {
        printf("Action %s is not supported.\nEnter %s help for help.\n",
               CMD, PROG_NAME);
        return -1;
    }

    args->cmd_cb = g_commands[i].cmd_cb;
    args->filepath = FILEPATH;

    return 0;
}

int cmd_help(const char *progname)
{
    char msg[] = 
        "Usage: %s action filepath\n"           \
        "\n"                                    \
        "Where:\n"                              \
        "  action:\n"                           \
        "    help    : print this message\n"    \
        "    infohash: print the infohash of the torrent file\n" \
        "    pieces  : print the value of the 'pieces' key of the torrent file\n" \
        "    info    : print the value of the 'info' key the torrent file\n" \
        "    print   : print the torrent file\n" \
        "    peers   : print information about the peers of the torrent file\n" \
        "\n"                                    \
        "  filepath: path to the torrent file\n";
    printf(msg, progname);

    return 0;
}

int cmd_infohash(const char *filepath)
{
    struct BData *bd = wm_bdata_new(NULL, 0);
    if(wm_bfile_read(filepath, bd) < 0)
        return -1;

    struct BNode *broot = wm_bdecode(bd);

    if(broot == NULL)
        return -1;
    
    struct BNode *infoNode = wm_bnode_get_bnode_by_key(broot, "info");
    if(infoNode == NULL)
    {
        printf("infoNode is null\n");
        return -1;
    }

    byte infoHash[SHA_DIGEST_LENGTH];
    SHA1(infoNode->data.data, infoNode->data.sz, infoHash);

    for(int i = 0; i < SHA_DIGEST_LENGTH; ++i)
        printf("%x", infoHash[i]);
    printf("\n");

    wm_bnode_delete(broot);
    wm_bdata_delete(bd);

    return 0;
}

int cmd_pieces(const char *filepath)
{
    struct BData *bd = wm_bdata_new(NULL, 0);;
    if(wm_bfile_read(filepath, bd) < 0)
        return -1;

    struct BNode *broot = wm_bdecode(bd);

    if(broot == NULL)
        return -1;
    
    struct BNode *infoNode = wm_bnode_get_bnode_by_key(broot, "info");
    if(infoNode == NULL)
    {
        printf("infoNode is null\n");
        return -1;
    }

    struct BNode *piecesNode = wm_bnode_get_bnode_by_key(infoNode, "pieces");
    if(piecesNode == NULL)
    {
        printf("piecesNode is null\n");
        return -1;
    }
    for(int i = 0; i < piecesNode->data.sz; ++i)
        printf("%c", piecesNode->data.data[i]);

    wm_bnode_delete(broot);
    wm_bdata_delete(bd);

    return 0;
}

int cmd_info(const char *filepath)
{
    struct BData *bd = wm_bdata_new(NULL, 0);
    if(wm_bfile_read(filepath, bd) < 0)
        return -1;

    struct BNode *broot = wm_bdecode(bd);

    if(broot == NULL)
        return -1;

    struct BNode *infoNode = wm_bnode_get_bnode_by_key(broot, "info");
    if(infoNode == NULL)
    {
        printf("infoNode is null\n");
        return -1;
    }

    for(int i = 0; i < infoNode->data.sz; ++i)
        printf("%c", infoNode->data.data[i]);

    wm_bnode_delete(broot);
    wm_bdata_delete(bd);

    return 0;
}

int cmd_print(const char *filepath)
{
    struct BData *bd = wm_bdata_new(NULL, 0);
    if(wm_bfile_read(filepath, bd) < 0)
        return -1;

    struct BNode *broot = wm_bdecode(bd);

    if(broot == NULL)
        return -1;

    struct key
    {
        char *str;
        int   is_optional;
    };

    const struct key metainfo_keys[] = 
    {
        {"announce", 0},
        {"announce-list", 1},
        {"comment", 1},
        {"created by", 1},
        {"creation date", 1},
        {"info", 0}        
    };
    const int metainfo_keys_sz = sizeof(metainfo_keys)/sizeof(struct key);

    const struct key single_file_info_keys[] =
    {
        {"length", 0},
        {"md5sum", 1},
        {"name", 0},
        {"piece length", 0},
        {"pieces", 0}
    };
    const int single_file_info_keys_sz = sizeof(single_file_info_keys)/sizeof(struct key);

    const struct key multi_file_info_keys[] =
    {
        {"files", 0},
        {"name", 0},
        {"piece length", 0},
        {"pieces", 0}
    };
    const int multi_file_info_keys_sz = sizeof(multi_file_info_keys)/sizeof(struct key);
    
    const struct key multi_file_info_files_keys[] =
    {
        {"length", 0},
        {"md5sum", 1},
        {"path", 0}
    };
    const int multi_file_info_files_keys_sz = sizeof(multi_file_info_files_keys)/sizeof(struct key);

    struct BNode *n = NULL;
    for(int i = 0; i < metainfo_keys_sz; ++i)
    {
        n = wm_bnode_get_bnode_by_key(broot, metainfo_keys[i].str);
        if(n == NULL)
        {
            if(!metainfo_keys[i].is_optional)
            {
                printf("Invalid metainfo file: key %s is missing\n", metainfo_keys[i].str);
                return -1;
            }
            else
            {
                continue;
            }
        }

        if(n->sibling != NULL)
        {
            char value[n->data.sz];
            memcpy(value, n->data.data, n->data.sz);
            printf("%s: %s\n", metainfo_keys[i].str, value); // TODO: extra chars at the end!!!
        }
        else if(n->child != NULL)
        {
            printf("%s: { here be dragons!! }\n", metainfo_keys[i].str);
        }
    }
    
    wm_bnode_delete(broot);
    wm_bdata_delete(bd);

    return 0;
}

int cmd_peers(const char *filepath)
{
    int rc = 0;

    wm_thp_init();

    thp_session_t *session = wm_thp_new_session();

    if(session != NULL)
    {
        struct BData *bd = wm_bdata_new(NULL, 0);
        if(wm_bfile_read(filepath, bd) < 0)
        {
            rc = -1;
            wm_bdata_delete(bd);
            goto cleanup;
        }

        struct BNode *broot = wm_bdecode(bd);

        if(broot == NULL)
        {
            rc = -1;
            wm_bdata_delete(bd);
            goto cleanup;
        }

        //==========announce_url==========
        struct BNode *announceNode = wm_bnode_get_bnode_by_key(broot, "announce");
        if(announceNode == NULL)
        {
            printf("announceNode is null\n");
            rc = -1;
            wm_bnode_delete(broot);
            wm_bdata_delete(bd);
            goto cleanup;
        }
        char announce_url[announceNode->data.sz];
        memcpy(announce_url, announceNode->data.data, announceNode->data.sz);
        announce_url[announceNode->data.sz] = '\0';
        printf("announce: %s\n", announce_url);

        //==========info_hash==========
        struct BNode *infoNode = wm_bnode_get_bnode_by_key(broot, "info");
        if(infoNode == NULL)
        {
            printf("infoNode is null\n");
            rc = -1;
            wm_bnode_delete(broot);
            wm_bdata_delete(bd);
            goto cleanup;
        }

        byte info_hash[SHA_DIGEST_LENGTH];
        SHA1(infoNode->data.data, infoNode->data.sz, info_hash);

        printf("info_hash: ");
        for(int i = 0; i < SHA_DIGEST_LENGTH; ++i)
            printf("%x", info_hash[i]);
        printf("\n");

        //==========peer_id==========
        // peer_id has to following form:
        // 19 18 17 16 15 14 13 12 ... 0
        //  -  W  M  0  0  0  0   random
        char peer_id[BT_HASH_LENGTH];
        memset(peer_id, 0, BT_HASH_LENGTH);
        strncpy(peer_id, "-WM0000", 7);

        const int rand_str_sz = 13;
        char rand_str[rand_str_sz];
        memset(rand_str, 0, rand_str_sz);
        get_rand_string(rand_str, rand_str_sz); // TODO: better random generator

        strncat(peer_id, rand_str, rand_str_sz);

        printf("peer_id: %s\n", peer_id);

        //==========port==========
        int port = 6881;

        //==========uploaded==========
        int uploaded = 0;

        //==========downloaded==========
        int downloaded = 0;

        //==========left==========
        int left = 100;

        //==========compact==========
        int compact = 1;

        wm_bnode_delete(broot);
        wm_bdata_delete(bd);

        struct THPRequest req = THP_REQUEST_INIT();
        memcpy(req.info_hash, info_hash, BT_HASH_LENGTH);
        memcpy(req.peer_id, peer_id, BT_HASH_LENGTH);
        req.port = port;
        req.uploaded = uploaded;
        req.downloaded = downloaded;
        req.left = left;
        req.compact = compact;

        struct THPResponse rsp = THP_RESPONSE_INIT();
        
        if(wm_thp_send(session, announce_url, &req, &rsp) == 0)
        {
            log_error("wm_thp_send failed");
            rc = -1;
            goto cleanup;
        }

        // TODO: print peers
    }

cleanup:
    wm_thp_delete_session(session);
    wm_thp_deinit();

    return rc;
}

static void get_rand_string(char *output, int sz)
{
    if(output == NULL)
        return;

    srand(time(0));
    
    int r0 = 1000000000 + (rand() % 9000000000);
    r0 *= (r0 < 0) ? -1 : 1;

    char s0[11];
    snprintf(s0, 11, "%d", r0);

    int r1 = 100 + (rand() % 900);
    r1 *= (r1 < 0) ? -1 : 1;

    char s1[4];
    snprintf(s1, 4, "%d", r1);

    strncpy(output, s0, 10);
    strncat(output, s1, 3);

    output[sz] = '\0';
}
