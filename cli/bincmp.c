#include <stdio.h>
#include "wiremesh-core/bfile.h"

int main(int argc, char *argv[])
{
  if(argc != 3)
    return -1;
  struct BData bd0 = {NULL, 0};
  if(wm_bfile_read(argv[1], &bd0) < 0)
    return -1;

  struct BData bd1 = {NULL, 0};
  if(wm_bfile_read(argv[2], &bd1) < 0)
    return -1;

  if(bd0.sz != bd1.sz)
  {
    printf("Files don't have the same size %d != %d\n", bd0.sz, bd1.sz);
    return -1;
  }

  int i;
  for(i = 0; bd0.data[i] == bd1.data[i] && i < bd0.sz; ++i);
  if(i == bd0.sz)
  {
    printf("Files are equal\n");
  }
  else
  {
    printf("Files are not equal: byte %d - %x != %x\n",
           i, bd0.data[i], bd1.data[i]);
  }
  return 0;
}
