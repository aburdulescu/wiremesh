cmake_minimum_required(VERSION 3.7.2)

project(wiremesh-cli)

set(INCPATH
  )

set(HEADERS
  )

set(SOURCES
  wiremesh-cli.c
  )

include_directories(${INCPATH})

add_executable(${PROJECT_NAME} ${HEADERS} ${SOURCES})

set(LIBS
  wiremesh
  crypto
  )

target_link_libraries(${PROJECT_NAME} ${LIBS})

install(TARGETS ${PROJECT_NAME} DESTINATION bin/)


