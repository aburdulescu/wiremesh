#!/bin/bash

#valgrind --leak-check=full $@ | tee valgrind.log
valgrind --tool=memcheck --leak-check=full --track-origins=yes --show-leak-kinds=all $@ | tee valgrind.log
