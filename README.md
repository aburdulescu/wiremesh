WireMesh is a BitTorrent client(work-in-progress).

## Install

The only way, for now, to install is by building the sources.

### On Linux(Debian/Ubuntu)

If you don't use sudo then login as root and enter the commands without sudo.

#### Install needed build tools
Update package repository:
```
sudo apt-get update
```

Install packages:
gcc, make
```
sudo apt-get install build-essential
```

cmake
```
sudo apt-get install cmake
```

#### Compile the project

Clone the repository, then:
```
cd lib
mkdir build && cd build
cmake ..
make
```

#### Install library

```
sudo make install
```

#### Refresh ldconfig cache(the linker will not know about the lib otherwise)

```
sudo ldconfig
```

## Usage

The project is a work-in-progress, so usage is limited for now.
However, in the tools directory of the project there is a cli tool(and a web interface will be developed when the project will be relativelly stable) to which, with time, more features will be added and can/will be used to interact with the code.

### Install dependencies:
OpenSSL:
```
sudo apt-get install libssl-dev
```

### Compile

```
cd cli
mkdir build && cd build
cmake ..
make
```

### Install:

```
sudo make install
sudo ldconfig
```

### Run

First consult tool help message to see what the tool can do and how to use it:
```
wiremesh-cli help
```

Then, for example, print the infohash of the torrent file provided as a agrument:
```
wiremesh-cli infohash /path/to/torrentfile
```

## Contributing

TBD

## Project coding style

See [c_coding_style.md](doc/c_coding_style.md)

## License

This project is licensed under the GPLv3 License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

Third-party code:
[rxi/log.c library](https://github.com/rxi/log.c)
