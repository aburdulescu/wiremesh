#!/bin/bash

DIR=../lib
PAHOLE_OUTPUT_DIR=$DIR/pahole_output

rm -rf $PAHOLE_OUTPUT_DIR
mkdir -p $PAHOLE_OUTPUT_DIR

cd $DIR

FILES=$(find -type f -name "*.o")

for f in $FILES
do
    pahole $f > $PAHOLE_OUTPUT_DIR/$(basename $f).pahole
done
