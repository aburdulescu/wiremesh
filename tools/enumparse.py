import sys
import os


def parse_file(path):
    with open(path, 'r') as f:
        data = f.read().replace('\n', '').replace(' ', '')

        ENUM_STR = 'enum'
        ENUM_STR_SZ = len(ENUM_STR)

        enums = {}

        enum_idx = data.find(ENUM_STR)

        # print('enum at data[%d:]' % enum_idx)

        brace_idx = data.find('{', enum_idx+ENUM_STR_SZ)

        # print('first "{" at: %d' % brace_idx)

        enum_name = data[enum_idx:brace_idx]
        # print(enum_name)

        list_idx = data.find('}', brace_idx+1)

        enum_list = data[brace_idx+1:list_idx].split(',')
        # print(enum_list)

        enums[enum_name] = enum_list

        print(enums)

def validate_args(args):
    if not len(args) == 2:
        print('%s needs one argument(i.e. the file to parse).' % args[0])
        sys.exit(1)
    if not os.path.isfile(args[1]):
        print('%s is not a file.' % args[1])

if __name__ == '__main__':
    validate_args(sys.argv)
    parse_file(sys.argv[1])
