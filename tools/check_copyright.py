#!/usr/bin/env python3

"""
Finds all C source files(i.e. *.[ch]) and
checks if they have copyright notice(i.e. if contains the string 'copyright').
"""

import argparse
import sys
import os


def parse_args():

    parser = argparse.ArgumentParser(
        description='Check files for copyright notice.')
    parser.add_argument('directory', help='the directory to check')

    args = parser.parse_args()

    if os.path.isdir(args.directory):
        return args.directory
    else:
        print('%s is not a directory' % args.directory)
        sys.exit(1)


def find_source_files(dir):

    source_files = []

    for root, dirs, files in os.walk(dir):
        for f in files:
            if f.endswith('.c') or f.endswith('.h'):
                fullpath = os.path.join(root, f)
                source_files.append(fullpath)

    return source_files


def has_copyright(filepath):

    with open(filepath, 'r') as f:
        for line in f:
            if 'copyright' in line.lower():
                return True

    return False


def get_files_without_copyright(source_files):

    for f in source_files:
        if not has_copyright(f):
            print(f)


if __name__ == '__main__':

    dir = parse_args()

    source_files = find_source_files(dir)

    get_files_without_copyright(source_files)
