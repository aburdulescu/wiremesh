#!/bin/bash

TRACEFILE=test.info
COVREPORT_DIR=covreport

TMP_OUTFILE=out.txt

build()
{
    make distclean 1>>$TMP_OUTFILE

    if [[ $1 == "-r" ]]; then
        make utest 1>>$TMP_OUTFILE
    fi
}

run()
{
    ./test 1>>$TMP_OUTFILE
}

run_lcov()
{
    TEST_SOURCE_FILE=$(pwd)/test.c
    lcov -c -d . -o $TRACEFILE 1>>$TMP_OUTFILE
    lcov -r $TRACEFILE "$TEST_SOURCE_FILE" -o $TRACEFILE 1>>$TMP_OUTFILE
}

gen_report()
{
    genhtml $TRACEFILE -o $COVREPORT_DIR --legend 1>>$TMP_OUTFILE
}


main()
{
    case $1 in
        "-r")
            if [[ -z $2 ]];then

                rm -rf $COVREPORT_DIR
                
                MODULES=$(ls -d */)
                for module in $MODULES
                do
                    echo "Current module: $module"
                    pushd $module >> /dev/null
                    
                    build $1
                    run
                    run_lcov
                    gen_report

                    popd >> /dev/null

                    if [[ -f $TRACEFILE ]]; then
                        lcov -a $TRACEFILE -a $module$TRACEFILE -o $TRACEFILE 1>>$TMP_OUTFILE
                    else
                        lcov -a $module$TRACEFILE -o $TRACEFILE 1>>$TMP_OUTFILE
                    fi
                done

                gen_report
            fi
            ;;
        "-c")
            rm -rf $COVREPORT_DIR
            rm -f $TRACEFILE
            rm -f $TMP_OUTFILE

            MODULES=$(ls -d */)
            for module in $MODULES
            do
                echo "Current module: $module"
                pushd $module >> /dev/null

                build $1
                rm -rf $COVREPORT_DIR
                rm -f $TRACEFILE
                rm -f $TMP_OUTFILE

                popd >> /dev/null
            done
            ;;
        *)
            echo "Unknown option: $1"
            exit -1
            ;;
    esac
}  

main $@
