#include "bdecode.h"
#include "testutils.h"
#include <stdlib.h>
#include <string.h>


void test_decstr();
void test_decstr_null_content();
void test_decstr_first_charachter_not_a_digit();

void test_decint();

void test_bdecode_int_str();
void test_bdecode_list_dict();

int
main(int argc, char* argv[])
{
    test_decstr_null_content();
    test_decstr_first_charachter_not_a_digit();
    test_decstr();

    test_decint();

    test_bdecode_int_str();
    test_bdecode_list_dict();

    return 0;
}

void test_decstr()
{
    START_TEST();

    buf_t content[] = "11:tracker.comi1234e";
    idx_t idx = 0;
    bufptr_t data = NULL;
    int data_sz = 0;

    printf("Torrent file content: %s\n", content);

    error_e ret = bdecode_str(content,
                              &idx,
                              &data,
                              &data_sz);

    bdata d = {data, data_sz};
    bufptr_t temp = bnode_getStringFromBdata(d);

    printf("data: %s, data_sz: %d, idx: %d\n", temp, data_sz, idx);

    free(temp);

    END_TEST(ret == ERR_NO_ERROR);
}

void test_decstr_null_content()
{
    START_TEST();

    bufptr_t content = NULL;
    idx_t idx = 0;
    bufptr_t data = NULL;
    int data_sz = 0;

    error_e ret = bdecode_str(content,
                              &idx,
                              &data,
                              &data_sz);

    END_TEST(ret == ERR_NULL_POINTER);
}

void test_decstr_first_charachter_not_a_digit()
{
    START_TEST();

    buf_t content[] = "a:abc";
    idx_t idx = 0;
    bufptr_t data = NULL;
    int data_sz = 0;

    error_e ret = bdecode_str(content,
                              &idx,
                              &data,
                              &data_sz);

    END_TEST(ret == ERR_BAD_BENCODE_ELEMENT);
}

void test_decint()
{
    START_TEST();

    buf_t content[] = "i12345e4:abcd";
    idx_t idx = 0;
    bufptr_t data = NULL;
    int data_sz = 0;

    printf("Torrent file content: %s\n", content);

    error_e ret = bdecode_int(content,
                              &idx,
                              &data,
                              &data_sz);

    bdata d = {data, data_sz};
    bufptr_t temp = (bufptr_t)malloc(sizeof(buf_t) * d.sz);
    bnode_getdata(&temp, d);

    printf("data: %s, data_sz: %d, idx: %d\n", temp, data_sz, idx);

    free(temp);
    END_TEST(ret == ERR_NO_ERROR);
}

void test_bdecode_int_str()
{
    START_TEST();

    buf_t content[] = "d4:key0i12345e4:key14:abcde";

    bnode *root = bdecode(content);

    printf("Bencode file content:\n");
    printf("%s\n\n", content);
    
    printf("Bencode tree contents:\n");

    bnode_print(root);


    bnode* v0 = bnode_getvalue(root, "key0");
    bufptr_t temp0 = (bufptr_t)malloc(sizeof(buf_t) * v0->data.sz);
    bnode_getdata(&temp0, v0->data);
    printf("Value of key \"key0\": %s\n", temp0);

    bnode* v1 = bnode_getvalue(root, "key1");
    bufptr_t temp1 = (bufptr_t)malloc(sizeof(buf_t) * v1->data.sz);
    bnode_getdata(&temp1, v1->data);
    printf("Value of key \"key1\": %s\n", temp1);

    bnode* v2 = bnode_getvalue(root, "dummy");
    printf("Does the key 'dummy' exists? %s\n",
           (v2 == NULL) ? "No" : "Yes");

    END_TEST(root != NULL);
}

void test_bdecode_list_dict()
{
    START_TEST();

    buf_t content[] = "d4:key0i12345e4:infod4:key1l4:str0eee";

    bnode *root = bdecode(content);

    printf("Bencode file content:\n");
    printf("%s\n\n", content);
    
    printf("Bencode tree contents:\n");
    bnode_print(root);

    END_TEST(root != NULL);
}
