#include "fileutils.h"

#define FILEPATH "/home/aab/projects/torrentliber/test/test.torrent"

int
main(int argc, char* argv[])
{
    bufptr_t buf = NULL;

    read_file(FILEPATH, &buf);
    if(buf)
        printbuf(buf);

    return 0;
}
