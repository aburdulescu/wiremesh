#ifndef __TESTUTILS_H__
#define __TESTUTILS_H__

#define PRINT_RESULT(cond)									\
	printf("Test result: %s\n", (cond)?"passed":"failed")

#define START_TEST()												\
	printf("\n==================START TEST==================\n");	\
	printf("Test name: %s\n", __PRETTY_FUNCTION__);					\
	printf("==============================================\n\n")	\

#define END_TEST(cond)												\
	printf("\n==============================================\n");	\
	PRINT_RESULT(cond);												\
	printf("==================END TEST====================\n")		\


#endif
